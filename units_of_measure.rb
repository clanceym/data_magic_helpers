module UnitsOfMeasure
  def rand_amount(type = 'metric')
    'mol'
  end
  alias_method :rand_metric_amount, :rand_amount

  def rand_imperial_amount
   rand_amount('imperial')
  end

  def rand_area(type = 'metric')
    { 'imperial' => ['acre', 'ft2', 'mi2'], 'metric' => ['ha', 'km2', 'm2'] }[type].sample
  end
  alias_method :rand_metric_area, :rand_area

  def rand_imperial_area
   rand_area('imperial')
  end

  def rand_consumption(type = 'metric')
    'mi/gal'
  end
  alias_method :rand_metric_consumption, :rand_consumption

  def rand_imperial_consumption
   rand_consumption('imperial')
  end

  def rand_density(type = 'metric')
    ['g/L', 'kg/m3'].sample
  end
  alias_method :rand_metric_density, :rand_density

  def rand_imperial_density
   rand_density('imperial')
  end

  def rand_electric_capacity(type = 'metric')
    ['A', 'C', 'V'].sample
  end
  alias_method :rand_metric_electric_capacity, :rand_electric_capacity

  def rand_imperial_electric_capacity
   rand_electric_capacity('imperial')
  end

  def rand_energy(type = 'metric')
    ['J', 'KJ', 'kW/h', 'MJ', 'N'].sample
  end
  alias_method :rand_metric_energy, :rand_energy

  def rand_imperial_energy
   rand_energy('imperial')
  end

  def rand_frequency(type = 'metric')
    'Hz'
  end
  alias_method :rand_metric_frequency, :rand_frequency

  def rand_imperial_frequency
   rand_frequency('imperial')
  end

  def rand_length(type = 'metric')
    { 'imperial' => ['ft', 'in', 'mi', 'mil', 'yd'], 'metric' => ['cm', 'km', 'm', 'Mm', 'mm', 'μm'] }[type].sample
  end
  alias_method :rand_metric_length, :rand_length

  def rand_imperial_length
   rand_length('imperial')
  end

  def rand_luminousity(type = 'metric')
    'cd'
  end
  alias_method :rand_metric_luminousity, :rand_luminousity

  def rand_imperial_luminousity
   rand_luminousity('imperial')
  end

  def rand_magnetism(type = 'metric')
    ['G', 'mT', 'T', 'Wb'].sample
  end
  alias_method :rand_metric_magnetism, :rand_magnetism

  def rand_imperial_magnetism
   rand_magnetism('imperial')
  end

  def rand_mass(type = 'metric')
    { 'imperial' => ['lb', 'oz', 'st'], 'metric' => ['g', 'kg', 'mcg', 'mg', 't'] }[type].sample
  end
  alias_method :rand_metric_mass, :rand_mass

  def rand_imperial_mass
   rand_mass('imperial')
  end

  def rand_power(type = 'metric')
    ['hp', 'kW', 'W'].sample
  end
  alias_method :rand_metric_power, :rand_power

  def rand_imperial_power
   rand_power('imperial')
  end

  def rand_pressure(type = 'metric')
    { 'imperial' => ['Psi'], 'metric' => ['BAR', 'kPa', 'Pa'] }[type].sample
  end
  alias_method :rand_metric_pressure, :rand_pressure

  def rand_imperial_pressure
   rand_pressure('imperial')
  end

  def rand_speed(type = 'metric')
    { 'imperial' => ['mi/h'] , 'metric' => ['km/h', 'm/s'] }[type].sample
  end
  alias_method :rand_metric_speed, :rand_speed

  def rand_imperial_speed
   rand_speed('imperial')
  end

  def rand_temp(type = 'metric')
    { 'imperial' => ['F', '°F'], 'metric' => ['C', '°C'] }[type].sample
  end
  alias_method :rand_metric_temp, :rand_temp

  def rand_imperial_temp
   rand_temp('imperial')
  end

  def rand_time(type = 'metric')
    ['d', 'h', 'min', 's'].sample
  end
  alias_method :rand_metric_time, :rand_time

  def rand_imperial_time
   rand_time('imperial')
  end

  def rand_volume(type = 'metric')
    { 'imperial' => ['ft3', 'gal', 'in3', 'pt', 'qt', 'yd3'], 'metric' => ['cm3', 'l', 'm3', 'ml'] }[type].sample
  end
  alias_method :rand_metric_volume, :rand_volume

  def rand_imperial_volume
   rand_volume('imperial')
  end
end
