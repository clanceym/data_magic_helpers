module StringStuff
  def incrementalist(start, base = nil)
    "#{base}#{start + 1}"
  end

  def concatalist(a, b)
    "#{a}#{b}"
  end
end
